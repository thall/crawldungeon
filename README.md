# Crawl Dungeon: Rock Stew
__Fight Dark Knights and enemy mages with fireballs in the dark dungeon!__ (spooky!)

2D-Graphical game written in Scala for studying purposes.
I don't think I am going to continue this project because I don't enjoy how the networking is setup using RMI
which limits playing to LAN networks and is at least in my opinion not worth diving in to find a workaround.

If you want to play download a release jar from the release folder and run it with `scala` or `java -jar` with either the argument `client` or `server` and then follow the prompts.
	

### [Watch a demo of the game here!](https://gnu3.xyz/files/demo-short.webm)
##### (There is no audio so missing the sound effects)

#### [Watch a live demo of the game! (out of date)](https://gnu3.xyz/files/networked-multiplayer.webm)

### Dependencies: (For Linux)
- Java 8
#### OpenJDK users:
- `java-openjfx`
- `ffmpeg-compat-54` OR `libavcodec54`, `libavformat54`, etc. (varies by distrobution)

### Screenshots
#### (bad quality; snapshots from the demo video linked above
##### spectator view:
![](images/maze2.png?raw=true)
##### player view:
![](images/maze.png?raw=true)

### FAQ:
- Q: What do I do when I get this error: "`Error. A JNI error has occurred, please check your installation and try again`"
- A: You are likely missing the JavaFX library. If you are on a GNU/Linux system your distribution likely has a package named something like `java-openjfx` you can install and on anything else... ehhh, not my problem.

- Q: What do I do when the game command-line output says "Missing proper audio library" on Linux?
- A: Your system needs `ffmpeg` codecs supported by JavaFX. You likely need either `ffmpeg-compat-54` or `libavcodec54` and `libavformat54` (varies by Linux distrobution)

- Q: Why do the enemies ~~spasm~~ all walk left like that after I die?
- A: After battle has been commenced the dark knights pray towards their temple of dusk far to the west.

- Q: Why do I get this exception: `Caused by: java.rmi.ConnectException: Connection refused to host: 127.0.1.1` ?
- A: If you are trying to connect to a server over the internet (not on your local network) this is the error you will get. I am not going to try supporting network play over the internet because Java's RMI is very poorly documented and this was only done for a class, sorry. A more technical explanation of the issue is that if you have a firewall between you and the server at either end issues will arise sense RMI is ingenious enough to choose an absolutely random port to actually send data over instead of using the naming/registry port you establish the connection with. Genius.
