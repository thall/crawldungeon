name := "GraphicalGame"
version := "1.0"
scalaVersion := "2.12.4"

// added to run a pre-main when packaged as a JAR
// which will run client or server based on
// command line arguments

jarName in assembly := "output1.jar"

mainClass in assembly := Some("graphicgame.CrawlDungeon")

// added to allow assembly to work
assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first

}
libraryDependencies ++= Seq(
  "org.scalafx" %% "scalafx" % "8.0.144-R12",
  "com.novocode" % "junit-interface" % "0.11" % Test,
  "org.scalactic" %% "scalactic" % "3.0.4",
  "org.scalatest" %% "scalatest" % "3.0.4" % "test"
)
