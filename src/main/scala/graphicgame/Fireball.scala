package graphicgame

import OrientationDir._
import OrientationSpin._
import EntityType._

class Fireball(private var _x:Double, private var _y:Double, private var _face:Orientation, level:Level, creator:Player) extends Entity {

  /**
    Keep in mind when working with the spin value of the fireball's orientation
    that the fireball's orientation does not necessarily have a non-null spin
    value. So everything dealing this this.cSpin must take this into account.
    Consider changing this to work in a more scala-like way using Option(Orientationspin.Value)
    instead in the future.

    **/

  private var here = true
  private var distTraveled = 0

  // get told to die
  def extinguish(target:EntityType.Value): Unit = {
    here = false
    val points = target match {
      case Player => 10
      case Enemy => 2
      case Swipe => 0
      case Roomba => 0
    }
    // give the player who got a hit some points
    this.spawner.getPoints(points)
  }
  import EntityType._

  def name: String = "fireball"

  def score:Int = 0
  def hp:Int = 0

  def entityType: EntityType.Value = Fireball

  def isHurt: Boolean = false

  def isAttacking: Boolean = false

  def face: OrientationDir.Value = _face.dir

  def cSpin: OrientationSpin.Value = _face.spin

  override def x: Double = _x

  override def y: Double = _y

  override def width(): Double = 0.2

  override def height(): Double = 0.2

  def spawner:Player = creator

  /**
    Spin mechanic below should have the fireball's trail curve
    after distTraveled==35 in the current orientation's spin
    direction.
    **/

  // move in the current direction
  override def update(delay: Double): Unit = {
    // varying swerve values depending on age/distance
    val swerve = if(distTraveled>=35) .4 else if(distTraveled>=25) .3 else if(distTraveled>=15) .1 else .0
    distTraveled+=1
    if(distTraveled>=15) this.cSpin match {
      case null => // just do nothing, don't spin
      case s:OrientationSpin.Value => { // s can be Right or Left
        /**
          Spin mechanic below should have the fireball's trail curve
          after distTraveled==15 in the current orientation's spin
          direction.
          Horizontal relative to forward motion.
          **/
        _face.dir match {
          case North => {
            if(s==Right) _x+=swerve
            if(s==Left) _x-=swerve
          }
          case South => {
            if(s==Right) _x+=swerve
            if(s==Left) _x-=swerve
          }
          case East => {
            if(s==Right) _y+=swerve
            if(s==Left) _y-=swerve
          }
          case West => {
            if(s==Right) _y+=swerve
            if(s==Left) _y-=swerve
          }
        }
      }
    }
    // Fireball's actual forward-movement done here
    _face.dir match {
      case North => _y-=.2
      case South => _y+=.2
      case East => _x+=.2
      case West => _x-=.2
    }
  }

  /**
    what happens when fireball is in a wall after moving
    Spoiler Alert: [[[[it dies]]]]
    * */
  override def postCheck(): Unit = {
    if(!level.maze.isClear(_x,_y,this.width,this.height)) {
      here=false
    }
  }

  override def stillHere(): Boolean = {
    here
  }
}
