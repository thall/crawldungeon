package graphicgame

import OrientationDir._
import EntityType._

class FireBurst(private var _x:Double, private var _y:Double, private var _face:Orientation, level:Level, creator:Player) extends Entity {

  private var here = true

  // get told to die
  def extinguish(target:EntityType.Value): Unit = {
    here = false
    val points = target match {
      case Player => 20
      case Enemy => 4
      case Swipe => 0
      case Roomba => 0
    }
    // give the player who got a hit some points
    this.spawner.getPoints(points)
  }
  import EntityType._

  def name: String = "fireburst"

  def score:Int = 0
  def hp:Int = 0

  def entityType: EntityType.Value = FireBurst

  def isHurt: Boolean = false

  def isAttacking: Boolean = false

  def face: OrientationDir.Value = _face.dir

  override def x: Double = _x

  override def y: Double = _y

  override def width(): Double = 0.5

  override def height(): Double = 0.5

  def spawner:Player = creator

  // move in the current direction
  override def update(delay: Double): Unit = {
    // FireBurst's actual forward-movement done here
    _face.dir match {
      case North => _y-=.2
      case South => _y+=.2
      case East => _x+=.2
      case West => _x-=.2
    }
  }

  /**
    what happens when FireBurst is in a wall after moving
    Spoiler Alert: [[[[it dies]]]]
    * */
  override def postCheck(): Unit = {
    if(!level.maze.isClear(_x,_y,this.width,this.height)) {
      here=false
    }
  }

  override def stillHere(): Boolean = {
    here
  }
}
