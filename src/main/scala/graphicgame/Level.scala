package graphicgame

import OrientationDir._
import EntityType._

class Level(maze: Maze) {
  private var _entities: Seq[Entity] = Seq()

  def buildPassable(): PassableLevel = {
    new PassableLevel(maze, this.entities.map(e => new PassableEntity(e.x,e.y,e.face,e.width,e.height,e.entityType,e.isAttacking,e.isHurt,e.name,e.score,e.hp)))
  }

  def +=(e: Entity): Unit = {
    _entities = _entities :+ e
  }

  def -=(e: Entity): Unit = {
    _entities = _entities.filterNot(e == _)
  }

  def entities(): Seq[Entity] = {
    _entities
  }

  def maze(): Maze = {
    this.maze
  }

  def players(): Seq[Player] = entities.collect {
    case p: Player => p
  }

  def knights(): Seq[Enemy] = entities.collect {
    case e: Enemy => e
  }

  def fireballs(): Seq[Fireball] = entities.collect {
    case f: Fireball => f
  }

  def firebursts(): Seq[FireBurst] = entities.collect {
    case f: FireBurst => f
  }

  def swipes(): Seq[Swipe] = entities.collect {
    case s: Swipe => s
  }

  def roombas(): Seq[Roomba] = entities.collect {
    case r: Roomba => r
  }

  // NOTE: This and spawnPlayer almost identitcal except spawnPlayer returns a Player
  def spawnEntity(eType:EntityType.Value): Unit = {
    val r = new scala.util.Random
    var spawned = false
    val yLimit = maze.height
    val xLimit = maze.width
    var spawnX:Double = r.nextInt(xLimit).toDouble
    var spawnY:Double = r.nextInt(yLimit).toDouble
    val width = eType match{
      case Enemy => 1.2
      case Roomba => 1.8
    }
    val height = eType match{
      case Enemy => 0.9
      case Roomba => 1.8
    }
    /**
      Find a valid spawn location randomly.
      Checks:
      - Is there a wall there?
      - Is there an entity there?
      * */
    while(!spawned){
      spawnX = math.random*xLimit
      spawnY = math.random*yLimit
      if(maze.isClear(spawnX,spawnY,width,height)) {
        spawned = true
      }
    }
    // Make a random orientation direction
    val faceNum = r.nextInt(3)
    val newFace = faceNum match {
      case 0 => new Orientation(North)
      case 1 => new Orientation(South)
      case 2 => new Orientation(East)
      case 3 => new Orientation(West)
    }
    // create the enemy
    val newEntity = eType match {
      case Enemy => new Enemy(newFace,spawnX,spawnY,this)
      case Roomba => new Roomba(newFace,spawnX,spawnY,this)
    }
    // add the enemy to the level
    this += newEntity
  }

  def spawnPlayer(name:String,activePlayer:Boolean): Player = {
    val r = new scala.util.Random
    val yLimit = maze.height
    val xLimit = maze.width
    var spawnX:Double = math.random*xLimit
    var spawnY:Double = math.random*yLimit
    var spawned = false
    val width = 1.0
    val height = 1.0
    /**
      Find a valid spawn location randomly.
      Checks:
      - Is there a wall there?
      - Is there an entity there?
      * */
    val faceNum = r.nextInt(3)
    val newFace = faceNum match {
      case 0 => new Orientation(North)
      case 1 => new Orientation(South)
      case 2 => new Orientation(East)
      case 3 => new Orientation(West)
    }
    var newPlayer:Player = null
    while(!spawned){
      spawnX = math.random*xLimit
      spawnY = math.random*yLimit
      if(maze.isClear(spawnX,spawnY,width,height)) {
        spawned = true
        newPlayer = new Player(name, newFace, spawnX, spawnY, this)
      }
    }
    // Make a random orientation direction
    // create the enemy
    // add the enemy to the level
    if(activePlayer) this += newPlayer
    newPlayer
  }

  /** NOTE: Next three functions share a ton in common, reduce overlaps **/
  /**
    Check if two entities overlap.
    Just treating every entity like a rectangle
    for simplicity's sake **/
  def isOverlap(e1:Entity,e2:Entity):Boolean = {
    val toprightx1 = e1.x + e1.width/2
    val toprighty1 = e1.y + e1.height/2
    val toprightx2 = e2.x + e2.width/2
    val toprighty2 = e2.y + e2.height/2
    val bottomleftx1 = e1.x - e1.width/2
    val bottomlefty1 = e1.y - e1.height/2
    val bottomleftx2 = e2.x - e2.width/2
    val bottomlefty2 = e2.y - e2.height/2
    if(bottomleftx2 > toprightx1 || bottomlefty2 > toprighty1 ||
         bottomleftx1 > toprightx2 || bottomlefty1 > toprighty2) false else true
  }

  /**
    Check if an area is clear of any entities
    This does not seem to work for some reason so I never use it.
**/
  def areaClear(x:Double,y:Double,width:Double,height:Double):Boolean = {
    var isClear = true
    for(e <- entities;if(isClear==true)) {
      val toprightx1 = e.x + e.width/2
      val toprighty1 = e.y + e.height/2
      val toprightx2 = x + width/2
      val toprighty2 = y + height/2
      val bottomleftx1 = e.x - e.width/2
      val bottomlefty1 = e.y - e.height/2
      val bottomleftx2 = x - width/2
      val bottomlefty2 = y - height/2
      if(bottomleftx2 > toprightx1 || bottomlefty2 > toprighty1 ||
           bottomleftx1 > toprightx2 || bottomlefty1 > toprighty2) isClear = false
    }
    isClear
  }

  /**
    Check if an area is clear of a specific entity type.
    Yeah it is basically the same as above but I am lazy
    **/
  def areaClearOf(x:Double,y:Double,width:Double,height:Double,etype:EntityType.Value):Boolean = {
    var isClear = true
    for(e <- entities;if(isClear==true && e.entityType==etype)) {
      val toprightx1 = e.x + e.width/2
      val toprighty1 = e.y + e.height/2
      val toprightx2 = x + width/2
      val toprighty2 = y + height/2
      val bottomleftx1 = e.x - e.width/2
      val bottomlefty1 = e.y - e.height/2
      val bottomleftx2 = x - width/2
      val bottomlefty2 = y - height/2
      if(bottomleftx2 > toprightx1 || bottomlefty2 > toprighty1 ||
           bottomleftx1 > toprightx2 || bottomlefty1 > toprighty2) isClear = false
    }
    isClear
  }

  /**
    Check for code to run each cycle
    - Call update on all entities
    - Call postcheck on all entities
    - Check if any entities need to be
    de-rezzed (!e.stillHere)
    **/

  def updateAll(delay: Double): Unit = {
    for (e <- entities)
      e.update(delay)
    for (e <- entities)
      e.postCheck()
    for (e <- entities) {
      if(!e.stillHere) this.-=(e)
    }

    /**
      Entity Collision-Checking below
      Checks for maze wall collisions are done
      per-entity in their respective .postCheck()s
      **/

    for (p <- players){
      for(s <- swipes;if(isOverlap(s,p))){
        s.extinguish
        p.impact(s.face,"nonoverlap")
      }
      for(s <- fireballs;if(isOverlap(s,p) && s.spawner != p)){
        s.extinguish(Player)
        p.impact(s.face,"nonoverlap")
      }
      for(s <- firebursts;if(isOverlap(s,p) && s.spawner != p)){
        s.extinguish(Player)
        p.impact(s.face,"burst")
      }
      for(e <- knights; if isOverlap(e,p)) p.impact(e.face,"overlap")
      for(r <- roombas; if isOverlap(r,p)) p.impact(r.face,"overlap")
    }
    for(e <- knights){
      for (f <- fireballs;if(isOverlap(f,e))){
        f.extinguish(Enemy)
        e.impact(e.face,"fireball")
      }
      for (f <- firebursts;if(isOverlap(f,e))){
        f.extinguish(Enemy)
        e.impact(e.face,"burst")
      }
    }
    for (r <- roombas){
      for (f <- fireballs; if(isOverlap(f,r))){
        f.extinguish(Roomba)
        r.impact
      }
      for (f <- firebursts; if(isOverlap(f,r))){
        f.extinguish(Roomba)
        r.impact
      }
    }
    for (f <- fireballs; s <- swipes; if isOverlap(f,s)) f.extinguish(Swipe)
  }
}
