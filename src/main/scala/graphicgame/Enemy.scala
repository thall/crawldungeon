package graphicgame


import scala.collection.mutable.Queue

class Enemy(private var _face: Orientation, private var _x: Double, private var _y: Double, val level: Level)
    extends Entity {

  import OrientationDir._
  import EntityType._

  private var targetx,targety= 0.0
  private var _attackFace: OrientationDir.Value = South
  private var _isAttacking = false // currently thrusting sword?
  private var _attackPhase = 0 // delay for the attack "animation"
  private var alive = true
  private var oldx = _x
  private var oldy = _y
  private var prevFace = _face
  private var armor = 20 // health
  private var hurt = false

  override def stillHere(): Boolean = alive
  override def x: Double = _x
  override def y: Double = _y
  override val height: Double = 1.0
  override val width: Double = 0.8

  def name:String = "enemy"
  def entityType: EntityType.Value = Enemy
  def attackFace: OrientationDir.Value = _attackFace
  def face: OrientationDir.Value = _face.dir
  def attackPhase:Int = _attackPhase
  def isAttacking: Boolean = _isAttacking
  def hp:Int = armor
  def score:Int = 0
  def isHurt: Boolean = hurt


  val offsets = List((1.0, 0.0), (-1.0, 0.0), (0.0, 1.0), (0.0, -1.0))

  /** Find distance to given coordinates (usually an enemy player) **/
  private def bfs(maze:Maze,sx:Double,sy:Double,ex:Double,ey:Double): Double = {
    val queue = new Queue[(Double, Double, Int)]
    var visited = scala.collection.mutable.Set[(Double,Double)]()
    if(!isValid(maze,sx,sy)) 420000
    else {
      queue.enqueue((sx,sy,0))
      visited += (sx -> sy)
      while(!queue.isEmpty) {
        val (x,y,steps) = queue.dequeue()
        for((dx,dy) <- offsets) {
          val (nx,ny) = (x+dx,y+dy)
          if(((nx-ex).abs<1 && (ny-ey).abs<1)) return steps+1
          if(isValid(maze,nx,ny) && !visited(nx -> ny)){
            queue.enqueue((nx,ny,steps+1))
            visited += (nx -> ny)
          }}
      }
      420001
    }
  }
  /** additional code that gets used above **/
  private def isValid(maze:Maze,x:Double,y:Double):Boolean = {
    if (maze.isClear(x,y,width,height)) true else false
  }

  private def move(nx:Double,ny:Double):Unit = {
    // width adjusted because width changes when attacking
    if (level.maze.isClear(nx+_x,ny+_y,width,height)) {
      _x += nx*.5
      _y += ny*.5
    }
  }
  /** when the enemy is informed it has been hit by the level: **/
  def impact(fFace:OrientationDir.Value,impactType:String) = {
    hurt = true
    _attackPhase = -18
    if(fFace == _face.opposite && armor>0){
      if(impactType=="fireball") armor -=1 else armor -=4
    } else if(armor>1) {
      if(impactType=="fireball") armor -=2 else armor -=6
    } else alive=false
  }

  /** fairly self-explanatory, begin an attack in a direction **/
  private def attack(aface:OrientationDir.Value):Unit = {
    _isAttacking = true
    _attackPhase = 1
    _attackFace = aface
  }

  override def update(delay:Double): Unit = {
    prevFace = _face
    hurt = false
    if(attackPhase<1) _isAttacking = false

    /**
      movement code which figures out where to go and
      whether the enemy should begin an attack based
      on bfs distance to a player.
      * */

    oldx = _x
    oldy = _y
    var closestPlayerN = 420001.0
    var closestPlayerS = 420001.0
    var closestPlayerE = 420001.0
    var closestPlayerW = 420001.0
    val around = 15
    /**
      Iterate through all players in the current level
      setting the closestPlayerX value to the bfs result
      if it is different from the current value and
      it is less that the current value so that the
      enemy moves in the direction of the closest player.

      **/
    for (p <- level.players) {
      targetx = p.x
      targety = p.y

      val bfsN = bfs(level.maze,_x,_y-1,targetx,targety)
      val bfsS = bfs(level.maze,_x,_y+1,targetx,targety)
      val bfsE = bfs(level.maze,_x+1,_y,targetx,targety)
      val bfsW = bfs(level.maze,_x-1,_y,targetx,targety)
      if(bfsN != closestPlayerN && bfsN<closestPlayerN){
        closestPlayerN = bfsN
      }
      if(bfsS != closestPlayerS && bfsS<closestPlayerS){
        closestPlayerS = bfsS
      }
      if(bfsE != closestPlayerE && bfsE<closestPlayerE){
        closestPlayerE = bfsE
      }
      if(bfsW != closestPlayerW && bfsW<closestPlayerW){
        closestPlayerW = bfsW
      }
    }
    if(closestPlayerW<=closestPlayerE && closestPlayerW<=closestPlayerN && closestPlayerW<=closestPlayerS) {
      move(-delay,0)
      _face.chDir(West)
    }
    else if (closestPlayerE<=closestPlayerN && closestPlayerE<=closestPlayerS){
      move(delay,0)
      _face.chDir(East)
    }
    else if (closestPlayerN<=closestPlayerS) {
      move(0,-delay)
      _face.chDir(North)
    } else {
      move(0,delay)
      _face.chDir(South)
    }
    if(closestPlayerN < around-1 || closestPlayerS < around-1 || closestPlayerE < around-1 || closestPlayerW < around-1){
      // this prevents spasming in all directions when close to a player... sometimes
      _face = prevFace
    }
    // check to begin an attack
    if(closestPlayerN < around || closestPlayerS < around || closestPlayerE < around|| closestPlayerW < around){
      if(attackPhase<0) _attackPhase+=1
      if(attackPhase==0) {
        this.attack(this.face)
        level += new Swipe(_x,_y,_face.oClone,level)
      }
    }
  }

  override def postCheck(): Unit = {
    if(this.attackPhase >= 1){
      _attackPhase += 1
      if(_attackPhase>20) {
        _attackPhase= -200
      }
    }
  }

}
