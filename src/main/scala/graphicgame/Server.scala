package graphicgame

import scala.io.StdIn._
import java.rmi.server.UnicastRemoteObject

import OrientationDir._
import EntityType._


object Server extends UnicastRemoteObject with App with RemoteServer{

  println("set your hostname or just type localhost for LAN server")
  val hostname = readLine
  System.setProperty("java.rmi.server.hostname", hostname)
  println("server started")
  val maze = Maze(3, false, 20, 20, 0.85)
  val level = new Level(maze)

  // Entities to spawn on level-start:
  for (i <- 0 until 3) level.spawnEntity(Enemy)
  for(i <- 0 until 15) level.spawnEntity(Roomba)

  private var clients = List[RemoteClient]()
  // exists for simple disconnection exception handling
  private var cliNames = List[(RemoteClient,String)]()

  java.rmi.registry.LocateRegistry.createRegistry(1099)
  java.rmi.Naming.rebind("CrawlDungeonServer", this)

  /**
    Below also distinguishes between players and spectators
    .spawnPlayer(String,Bool) spawns a player with the Clientname
    and tells the level whether they should be part of the entities
    collection or not. (Spectators are not part of the collection).
    **/
  def connect(client:RemoteClient):RemotePlayer = {
    val clientName = client.getName()
    var taken = false // Liam Neeson
    if(cliNames.filter(x => x._2==clientName).length>0) {
      println("Somebody tried to connect with the name "+clientName+" which is taken.")
      println("They are now a spectator named: _"+clientName)
      taken = true
    }
    val rplayer = if(!clientName.contains("spectator") && !taken){
      clients::=client
      cliNames::=(client,clientName)
      println("the player "+clientName+" has connected")
      level.spawnPlayer(clientName,true)
    } else if (!taken){
      clients::=client
      cliNames::=(client,clientName)
      println("the spectator "+clientName+" has connected")
      level.spawnPlayer(clientName,false)
    } else {
      level.spawnPlayer("_"+clientName,false) // nothing renders here, but they deserve it
    }
    rplayer
  }

  /** basically handling network exceptions **/
  private def disconnect(c:RemoteClient):Unit = {
    val cliNamesNot = cliNames.filterNot(cli => cli._1==c)
    val disName = cliNames.diff(cliNamesNot)(0)._2
    val updated = clients.filterNot(client => c==client)
    clients = updated
    cliNames = cliNamesNot
    /**
      Below exists because spectators don't have players
      so it is necessary to check whether or not a client
      even has a player corresponding to them before trying
      to potentially index into an empty Sequence from
      filter which causes an exception.
      * */
    if(!level.players.filter(p => p.name==disName).isEmpty){
      val badPlayer = level.players.filter(p => p.name == disName)(0)
      level -= badPlayer
    }
    println(disName+" has disconnected")
  }

  // the level gets updated below
  var lastTime = 0L
  var refreshdelay = 0.0
  while (true) {
    val time = System.nanoTime()
    if (lastTime > 0) {
      val delay = (time - lastTime) / 1e9
      refreshdelay+=delay
      if(refreshdelay>=0.018){
        level.updateAll(refreshdelay)
        for (c <- clients){
          try { // handle missing clients
            c.updateLevel(level.buildPassable)
          } catch {
            case e: java.rmi.ConnectException => {
              disconnect(c)
            }
            case m: java.rmi.UnmarshalException => {
              disconnect(c)
            }
            case ex: Exception => {
              println(ex+" exception occured")
              System.exit(0)
            }
          }
        }
        refreshdelay = 0.0
      }
    }
    lastTime = time
  }
}
