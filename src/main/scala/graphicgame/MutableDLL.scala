package graphicgame

import collection.mutable

class MutableDLL[A] extends mutable.Buffer[A] {
  private var default: A = _
  private class Node(var data:A, var next: Node, var prev:Node)
  private val end: Node = new Node(default, null, null)
  end.prev = end
  end.next = end
  private var nElems = 0

  def length: Int = nElems

  def clear: Unit = {
    end.next = end
    end.prev = end
    nElems = 0
  }

  def apply(n: Int): A = {
    require(n>=0&&n<nElems)
    var rover = end.next
    for (i <- 1 to n) rover = rover.next
    rover.data
  }

  def update(n: Int , newData: A):Unit = {
    require(n>=0&&n<nElems)
    var rover = end.next
    for (i <- 1 to n) rover = rover.next
    rover.data = newData
  }

  def +=(newData: A): MutableDLL.this.type = {
    val newNode = new Node(newData,end,end.prev)
    end.prev.next = newNode
    end.prev = newNode
    nElems+=1
    this
  }

  def +=:(newData: A): MutableDLL.this.type = {
    println("DO NOT PREPEND TO THIS BUFFER TYPE, JUST STOP")
    this
  }
  
  def iterator: Iterator[A] = new Iterator[A] {
    var rover = end.next
    def hasNext: Boolean = {
      rover != end
    }
    def next: A = {
      val ret = rover.data
      rover = rover.next
      ret
    }
  }

  def remove(n: Int): A = {
    require(n>=0&&n<nElems)
    var rover = end.next
    for(i <- 0 until n ) rover = rover.next
    val ret = rover.data
    rover.prev.next = rover.next
    rover.next.prev = rover.prev
    nElems -= 1
    ret
  }

  def insertAll(n:Int, newElems: collection.Traversable[A]):Unit = {
    require(n>=0&&n<nElems)
    if (newElems.nonEmpty){
      var rover = end.next
      for (i <- 0 until n) rover = rover.next
      for (e <- newElems) {
        val newNode = new Node(e, rover, rover.prev)
        rover.prev.next = newNode
        rover.prev = newNode
        nElems+=1
      }
    }
  }
}
