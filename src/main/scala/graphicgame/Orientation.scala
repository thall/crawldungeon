package graphicgame

class Orientation(private var _dir: OrientationDir.Value, private var _spin:OrientationSpin.Value = null) {
  import OrientationDir._
  import OrientationSpin._

  /**
    Every entity should ultimately have Orientation.
    Orientation is just the direction the entity faces,
    which is why the variable for Orientation in an entity
    is called _face.

    Orientation can be set, cloned, read from, cycle
    clockwise and counterclockwise.

    Everything using North South East and West
    will need to have "import OrientationDir._"
    inside of it.

    Orientation objects can optionally store "spin"
    which is useful for projectiles and can be changed
    based on the current dir of the orientation

    * */

  def dir = _dir
  def spin = _spin
  def axisFace():Boolean = { // true is y axis, otherwise x axis
    if(this.dir == North || this.dir == South) true else false
  }

  def chDir(direction:OrientationDir.Value):Unit = {
    _dir = direction
  }

  def chSpin(newSpin:OrientationSpin.Value):Unit = {
    _spin = spin
  }

  def revSpin():Unit = {
    if(_spin == null) println("attempt to reverse a null spin value")
    else if(_spin == Right) _spin = Left:OrientationSpin.Value else _spin = Right:OrientationSpin.Value
  }

  /**
    This gives a clone of the current orientation.
    Useful for imparting identitical face onto an entity
    created within another entity. (projectiles)
    **/

  def oClone: Orientation = new Orientation(this.dir,this.spin)
  def fClone(spin:OrientationSpin.Value): Orientation = new Orientation(this.dir,spin)
  /**
    Both nwse and nesw rotate direction clockwise and
    counterclockwise respectively when they are called.
    These are used by the Roomba
    * */

  def nwse: Unit = _dir = this.getDir match { //cycle counter-clockwise
    case 1 => West
    case 2 => East
    case 3 => North
    case 4 => South
  }
  def nesw: Unit = _dir = this.getDir match { //cycle clockwise
    case 1 => East
    case 2 => West
    case 3 => North
    case 4 => South
  }
  def opposite: Unit = this._dir match {
    case North => this.chDir(South)
    case East => this.chDir(West)
    case West => this.chDir(East)
    case South => this.chDir(North)
  }
  // the stuff here with Integers is pretty painful and
  // redundant; should be removed at some point.
  private def getDir: Int = _dir match {
    case North => 1
    case South => 2
    case East => 3
    case West => 4
  }
}

// Just defining directions here, plz no touch

object OrientationDir extends Enumeration {
  val North, South, East, West = Value
}

object OrientationSpin extends Enumeration {
  val Right, Left = Value
}
