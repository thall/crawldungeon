package graphicgame

class Roomba(private var _face: Orientation, private var _x: Double, private var _y: Double, val level: Level)
    extends Entity {

  import OrientationDir._
  import EntityType._

  private var alive = true
  private var armor = 100
  private var hurt = false

  override def x: Double = _x
  override def y: Double = _y
  override def stillHere(): Boolean = alive
  override val width: Double = 1.0
  override val height: Double = 1.0

  def name:String = "roomba"
  def entityType: EntityType.Value = Roomba
  def face: OrientationDir.Value = _face.dir
  def isAttacking: Boolean = false
  def hp:Int = 0
  def score:Int = 0
  def isHurt: Boolean = hurt

  private def move(nx:Double,ny:Double):Unit = {
    // Distance to be from any walls
    val bufferSpace = 0.5
    if (level.maze.isClear(nx+_x,ny+_y,width+bufferSpace,height+bufferSpace)) {
      _x += nx*.5
      _y += ny*.5
    } else {
      this.boop()
    }
  }

  def boop():Unit = {
    // ROTATE THE SPIKY ROOMBA, WE'VE "BUMPED" A WALL
    val r = scala.util.Random
    // Rotate randomly until we stop colliding
    r.nextBoolean match {
      case true => _face.nwse
      case false => _face.nesw
    }
  }

  /** when the roomba is informed it has been hit by the level: **/
  def impact(): Unit = {
    hurt = true
    if(armor>0){
      armor -=1
    } else alive=false
  }

  override def update(delay:Double): Unit = {
    hurt = false
    // Move based on current direction
    this.face match {
      case North => move(0,-delay)
      case South => move(0,delay)
      case East => move(delay,0)
      case West => move(-delay,0)
    }
  }

  override def postCheck(): Unit = {}

}
