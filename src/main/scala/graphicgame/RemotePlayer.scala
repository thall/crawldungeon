package graphicgame

@remote trait RemotePlayer {
  // key pressed
  def moveUpP():Unit
  def moveDownP():Unit
  def moveLeftP():Unit
  def moveRightP():Unit
  def attackP():Unit
  // key released
  def moveUpR():Unit
  def moveDownR():Unit
  def moveLeftR():Unit
  def moveRightR():Unit
  def attackR():Unit
}
