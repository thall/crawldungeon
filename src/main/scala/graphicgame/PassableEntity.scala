package graphicgame

case class PassableEntity(x:Double,y:Double,face:OrientationDir.Value,width:Double,height:Double,
  entityType:EntityType.Value,isAttacking:Boolean,isHurt:Boolean,name:String,score:Int,hp:Int)
