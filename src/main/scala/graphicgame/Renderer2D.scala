package graphicgame

import EntityType._
import OrientationDir._

// NOTE: open-jfx (sometimes) needed on linux
import scalafx.scene.canvas.GraphicsContext
import scalafx.scene.image.Image
import scalafx.scene.paint.Color

// NOTE: libavcodec54, libavformat54, etc. OR ffmpeg-compat-54 optional on linux for sound
import scalafx.scene.media.{Media,MediaPlayer}
import java.io.File

/**
  * This is the 2D renderer that draws the game elements to a Canvas, creates the UI,
  * and plays the sounds.
  */
class Renderer2D(gc: GraphicsContext, blockSize: Double, pname:String) {
  private var lastCenterX = 0.0
  private var lastCenterY = 0.0
  /**
    Sprites:
    The sprites include seperate sprites for the direction faced,
    hurt status (oof), and whether attacking.

    located in src/main/resources/images
    * */

  private val floorImage = Renderer2D.loadImage("/images/floor.png")
  private val wallImage = Renderer2D.loadImage("/images/wall.png")
  private val enemyImageUpOof = Renderer2D.loadImage("/images/knight-up-oof.png")
  private val enemyImageDownOof = Renderer2D.loadImage("/images/knight-down-oof.png")
  private val enemyImageLeftOof = Renderer2D.loadImage("/images/knight-left-oof.png")
  private val enemyImageRightOof = Renderer2D.loadImage("/images/knight-right-oof.png")
  private val enemyImageUpAttack = Renderer2D.loadImage("/images/knight-up-attack.png")
  private val enemyImageDownAttack = Renderer2D.loadImage("/images/knight-down-attack.png")
  private val enemyImageLeftAttack = Renderer2D.loadImage("/images/knight-left-attack.png")
  private val enemyImageRightAttack = Renderer2D.loadImage("/images/knight-right-attack.png")
  private val enemyImageUp = Renderer2D.loadImage("/images/knight-up.png")
  private val enemyImageDown = Renderer2D.loadImage("/images/knight-down.png")
  private val enemyImageLeft = Renderer2D.loadImage("/images/knight-left.png")
  private val enemyImageRight = Renderer2D.loadImage("/images/knight-right.png")
  private val playerImageUpOof = Renderer2D.loadImage("/images/wizard-up-oof.png")
  private val playerImageDownOof = Renderer2D.loadImage("/images/wizard-down-oof.png")
  private val playerImageRightOof = Renderer2D.loadImage("/images/wizard-right-oof.png")
  private val playerImageLeftOof = Renderer2D.loadImage("/images/wizard-left-oof.png")
  private val playerImageUpCharging = Renderer2D.loadImage("/images/wizard-up-charging.png")
  private val playerImageDownCharging = Renderer2D.loadImage("/images/wizard-down-charging.png")
  private val playerImageRightCharging = Renderer2D.loadImage("/images/wizard-right-charging.png")
  private val playerImageLeftCharging = Renderer2D.loadImage("/images/wizard-left-charging.png")
  private val playerImageUp = Renderer2D.loadImage("/images/wizard-up.png")
  private val playerImageDown = Renderer2D.loadImage("/images/wizard-down.png")
  private val playerImageRight = Renderer2D.loadImage("/images/wizard-right.png")
  private val playerImageLeft = Renderer2D.loadImage("/images/wizard-left.png")
  private val fireballImage = Renderer2D.loadImage("/images/fireball.png")
  private val fireburstImage = Renderer2D.loadImage("/images/fireburst.png")
  private val swipeImageUp = Renderer2D.loadImage("/images/swipe-up.png")
  private val swipeImageDown = Renderer2D.loadImage("/images/swipe-down.png")
  private val swipeImageRight = Renderer2D.loadImage("/images/swipe-right.png")
  private val swipeImageLeft = Renderer2D.loadImage("/images/swipe-left.png")
  private val roombaImage = Renderer2D.loadImage("/images/roomba.png")
  private val roombaImageOof = Renderer2D.loadImage("/images/roomba-oof.png")

  /**
    Sounds:
    Sounds to be played, located in src/main/resources/sounds
    **/
  val oofSound = Renderer2D.loadMedia("/sounds/oof.mp3")

  /**
    * These two methods are used to figure out where to draw things. They are used by the render.
    */
  def blocksToPixelsX(bx: Double): Double =
    gc.canvas.getWidth / 2 + (bx - lastCenterX) * blockSize
  def blocksToPixelsY(by: Double): Double =
    gc.canvas.getHeight / 2 + (by - lastCenterY) * blockSize

  /**
    * These two methods are used to go from coordinates to blocks. You need them if you have mouse interactions.
    */
  def pixelsToBlocksX(px: Double): Double =
    (px - gc.canvas.getWidth / 2) / blockSize + lastCenterX
  def pixelsToBlocksY(py: Double): Double =
    (py - gc.canvas.getHeight / 2) / blockSize + lastCenterY

  /**
    * This method is called to render things to the screen.
    */
  def render(level: PassableLevel, cx: Double, cy: Double): Unit = {
    lastCenterX = cx
    lastCenterY = cy

    val drawWidth = (gc.canvas.getWidth / blockSize).toInt + 1
    val drawHeight = (gc.canvas.getWidth / blockSize).toInt + 1

    // Draw walls and floors
    for {
      x <- cx.toInt - drawWidth / 2 - 1 to cx.toInt + drawWidth / 2 + 1
      y <- cy.toInt - drawHeight / 2 - 1 to cy.toInt + drawHeight / 2 + 1
    } {
      val img = if (level.maze.apply(x, y)) {
        wallImage
      } else {
        floorImage
      }
      gc.drawImage(img,
                   blocksToPixelsX(x),
                   blocksToPixelsY(y),
                   blockSize,
                   blockSize)
    }

    // Draw entities
    for (e <- level.entities) {
      val img = e.entityType match {
        /**
          The sub-cases below display a different sprite for an entity
          based on:
          - current direction being faced
          - whether the entity is being damaged
          - whether the entity is attacking or charging an attack
          * */
        case Player =>
          if(e.isHurt) {
            e.face match {
              case North => playerImageUpOof
              case South => playerImageDownOof
              case East => playerImageRightOof
              case West => playerImageLeftOof
            }
          } else if(e.isAttacking) {
            e.face match {
              case North => playerImageUpCharging
              case South => playerImageDownCharging
              case East => playerImageRightCharging
              case West => playerImageLeftCharging
            }
          } else {
            e.face match {
              case North => playerImageUp
              case South => playerImageDown
              case East => playerImageRight
              case West => playerImageLeft
            }
          }
        case Enemy  =>
          if(e.isHurt) {
            e.face match {
              case South => enemyImageUpOof
              case North => enemyImageDownOof
              case West => enemyImageRightOof
              case East => enemyImageLeftOof
            }
          } else if(!e.isAttacking){
            e.face match {
              case North => enemyImageUp
              case South => enemyImageDown
              case East => enemyImageRight
              case West => enemyImageLeft
            }
          } else {
            e.face match {
              case North => enemyImageUpAttack
              case South => enemyImageDownAttack
              case East => enemyImageRightAttack
              case West => enemyImageLeftAttack
            }
          }
        case Swipe =>
          e.face match {
            case North => swipeImageUp
            case South => swipeImageDown
            case East => swipeImageRight
            case West => swipeImageLeft
          }
        case Fireball => fireballImage
        case FireBurst => fireburstImage
        case Roomba => if(e.isHurt) roombaImageOof else roombaImage
        case _ => fireballImage
      }
      gc.drawImage(img,
                   blocksToPixelsX(e.x - e.width / 2),
                   blocksToPixelsY(e.y - e.height / 2),
                   e.width * blockSize,
                   e.height * blockSize)
    }

    /**
      Player coordinates used data to draw level shadows,
      position health bars, nametags, etc. Enemy coords
      used for enemy health bars in spectator mode.
      * */

    val players:Seq[PassableEntity] = level.entities.filter(e => e.entityType == Player).sortWith(_.score>_.score)
    val enemies:Seq[PassableEntity] = level.entities.filter(e => e.entityType == Enemy)
    val cPlayerLocs = players.map(p => (p.x,p.y,p.name,p.isHurt,p.hp))
    val enemPlayerLocs = enemies.map(e => (e.x,e.y,e.hp))
    val playerNames:MutableDLL[(String,String)] = new MutableDLL[(String,String)]()
    for (p <- players) {
      playerNames.+=((p.name,p.score.toString))
    }
    val ourPlayer = {
      val p = players.filter(_.name == pname)
      if(p.length==1) Some(p(0)) else None
    }
    /**
      Level shadows: [The for-loop below this block]

      The general idea here is to create a sequence of points surrounding each player
      using their coordinates and then use these as the points for a polygon to be drawn
      which uses the four corners of the screen while also leaving clear the area surrounding
      them with four more points to make a box.

      Polygon Illustration:
      o----------------------------------------------------------o
      |..........................................................|
      |...............o---------o................................|
      |...............|         |................................|
      |...............|         |................................|
      8===============8    P    |..<==Player.....................|
      |double-line....|         |................................|
      |...............|         |................................|
      |...............o---------o................................|
      |..........................................................|
      |..........................................................|
      o----------------------------------------------------------o
      Where each "o" is a point and the "8"s are overlapping points
      (Note: made with M-x artist-mode in emacs, the best editor)
      This happens for each player individually using "their" renderer
      (based on name string) so it should scale to multiplayer absolutely fine.

      **/
    // player nametag and healthbar (part only in spectator mode)
    for ((px,py,playername,hurt,hp) <- cPlayerLocs) {
      val x = blocksToPixelsX(px)-20.0
      val y = blocksToPixelsY(py)+20.0
      val maxwidth = if (ourPlayer!=None) 40.0 else 100.0
      gc.fill = Color.White
      gc.fillText(playername,x,y,maxwidth)
      if(ourPlayer==None){
        for(i <- 0 until hp) {
          gc.fill = Color.Green
          //gc.fillRect(10.0,70.0+24*i,80.0,20.0)
          gc.fillRect(x+i*1.2,y+5.0,1.0,2.0)
        }
      }
    }
    // enemy health bars in spectator mode
    if(ourPlayer==None){
      for ((ex,ey,hp) <- enemPlayerLocs) {
        val x = blocksToPixelsX(ex)-13.0
        val y = blocksToPixelsY(ey)+10.0
        for(i <- 0 until hp) {
          gc.fill = Color.Green
          gc.fillRect(x+i*1.2,y,1.0,2.0)
        }
      }
    }
    /** SHADOW CODE:
      TODO: Maybe attempt to refactor in terms of blocksize an to allow different 
      screen sizes to be selected via a config file?
      * */
    val borderSize = 200
    for ((px,py,playername,hurt,hp) <- cPlayerLocs; if pname==playername) {
      var shadowPoints = Seq[(Double,Double)]((0.0,0.0))
      // code to convert maze locations of player to coordinates
      val x = blocksToPixelsX(px)
      val y = blocksToPixelsY(py)
      // make overlapping points to reach towards player
      shadowPoints = shadowPoints :+(0.0,y)
      shadowPoints = shadowPoints :+ (x-borderSize,y)
      // here is where the player-boundary points should be
      shadowPoints = shadowPoints :+ (x-borderSize,y-borderSize)
      shadowPoints = shadowPoints :+ (x+borderSize,y-borderSize)
      shadowPoints = shadowPoints :+ (x+borderSize,y+borderSize)
      shadowPoints = shadowPoints :+ (x-borderSize,y+borderSize)
      // make overlapping points to reach back towards window edge
      shadowPoints = shadowPoints :+ (x-borderSize,y)
      shadowPoints = shadowPoints :+ (0.0,y)
      shadowPoints = shadowPoints :+ (0.0,800.0)
      shadowPoints = shadowPoints :+ (1000.0,800.0)
      shadowPoints = shadowPoints :+ (1000.0,0.0)
      shadowPoints = shadowPoints :+ (0.0,0.0)
      // flash red and do an "OOF" when player is hurt, otherwise black
      if(hurt) {
        Renderer2D.playMedia(oofSound)
        gc.fill = Color.rgb(40,0,0,0.95)
      } else gc.fill = Color.rgb(0, 0, 0, 0.75)
      gc.fillRule = scalafx.scene.shape.FillRule.NonZero
      gc.fillPolygon(shadowPoints)
    }
    /** 
      Status sidebars:
      The vertical rectangles on each side of the screen.
      Right Side: Scoreboard/Alive Players
      Left Side: Version and Player Healthbar
      TODO: consider resizing in terms of blocksize
      **/
    // here is the vertical rectangle on the right side of the screen
    gc.fill = Color.Black
    gc.fillRect(900.0,0.0,100.0,800.0)
    gc.fill = Color.White
    gc.fillText("Alive Players:",905,20.0)
    // Scoreboard/Alive Players
    if(playerNames.length == 0) gc.fillText("NONE",905,40.0)
    var nameBoardy = 40
    for(n <- playerNames){
      gc.fillText(n._1+": "+n._2,910,nameBoardy)
      nameBoardy+=20
    }
    // here is the vertical rectangle of the left side of the screen
    gc.fill = Color.Black
    gc.fillRect(0.0,0.0,110.0,800.0)
    gc.fill = Color.Yellow
    gc.fillText("Crawl Dungeon:",2.0,20.0)
    gc.fillText("Rock Stew", 2.0,35.0)
    gc.fill = Color.White
    gc.fillText("v0.8",10.0,50.0)
    // healthbar
    if(ourPlayer!=None){
      for(i <- 0 until ourPlayer.get.hp) {
        gc.fill = Color.Green
        //gc.fillRect(10.0,70.0+24*i,80.0,20.0)
        gc.fillRect(10.0,760.0-24*i,80.0,20.0)
      }
    }
  }
}

object Renderer2D {

  /**
    * These methods assume that you are putting your images in src/main/resources and
    * that you are putting your sounds there as well
    * (media formats supported: https://docs.oracle.com/javafx/2/api/javafx/scene/media/package-summary.html#SupportedMediaTypes)
    * This directory is packaged into the JAR file. During development this will
    * go to the file in the directory structure if it can't find the resource
    * in the classpath. The argument should be the path inside of the resources directory.
    */

  def loadImage(path: String): Image = {
    val res = getClass.getResource(path)
    if (res == null) {
      new Image("file:src/main/resources" + path)
    } else {
      new Image(res.toExternalForm())
    }
  }
  def loadMedia(path: String): Media = {
    val res = getClass.getResource(path)
    val sysPath = System.getProperty("user.dir")
    if (res == null) {
      val newpath = sysPath+"/src/main/resources"+path
      new Media(new File(newpath).toURI.toString)
    } else {
      new Media(res.toExternalForm())
    }
  }
  def playMedia(media:Media): Unit = {
    try{
      val mediaP = new MediaPlayer(media)
      mediaP.play()
    } catch {
      case e:com.sun.media.jfxmedia.MediaException => println("Missing proper audio library")
    }
  }
}
