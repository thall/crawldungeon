package graphicgame

import java.rmi.server.UnicastRemoteObject
import OrientationDir._
import OrientationSpin._

class Player(private var _name: String,
             private var _face: Orientation,
             private var _x: Double,
             private var _y: Double,
             val level: Level)
    extends UnicastRemoteObject with Entity with RemotePlayer{

  import EntityType._

  private var spcReleased, spcPressed, leftPressed, rightPressed, downPressed, upPressed = false
  private var dirsPressed = 0 // number of direction keys pressed
  private var spcDelay = 0 // time space has been held
  private var oldy = _y
  private var oldx = _x
  private var fBallx = _x // where a new fireball should be created
  private var fBally = _y
  private var _fBallSpin:OrientationSpin.Value = null // spin to give new fireballs
  private var armor = 30 // health
  private var hurt = false
  private var attacking = false // whether player is charging a fireblast
  private var pscore = 0
  private val burstThreshold = 30 // charge delay before allowing a fireburst

  override def x: Double = _x
  override def y: Double = _y
  override val width: Double = 0.8
  override val height: Double = 0.8

  def name: String = _name
  def entityType: EntityType.Value = Player
  def face: OrientationDir.Value = _face.dir
  def isAttacking: Boolean = attacking
  def hp:Int = armor
  def score:Int = pscore
  def isHurt: Boolean = hurt
  def getPoints(pts:Int):Unit = pscore += pts
  private def fBallSpin = _fBallSpin

  override def update(delay: Double): Unit = {
    hurt = false
    // do a heal check
    heal(1)
    oldx = _x
    oldy = _y
    if (leftPressed == true) {
      _x -= 0.05
      dirsPressed+=1
    }
    if (rightPressed == true) {
      _x += 0.05
      dirsPressed+=1
    }
    if (upPressed == true) {
      _y -= 0.05
      dirsPressed+=1
    }
    if (downPressed == true) {
      _y += 0.05
      dirsPressed+=1
    }
    // do a fireball check
    if (spcPressed == true) {
      spcDelay+=1
      if(spcDelay>burstThreshold) attacking = true
    } else attacking = false
    fireballCheck()
  }

  /**
    The Fireball's Spin Direction is determined by checking the player's face
    and then checking to see what direction they are moving in on
    the other axis, if at all
    * */
  private def fireballCheck():Unit = {
    _fBallSpin = (_face.axisFace,leftPressed,rightPressed,downPressed,upPressed) match {
      case (true,true,false,_,_) => Left
      case (true,false,true,_,_) => Right
      case (false,_,_,true,false) => Right
      case (false,_,_,false,true) => Left
      case _ => null
    }
    val fballOffset = if (spcDelay<burstThreshold) 0.75 else 0.85 // how far away from center of player
    if (dirsPressed == 1) { // to spawn the fireball
      (leftPressed,rightPressed,downPressed,upPressed) match {
        case (true,false,false,false) => {
          _face.chDir(West)
          fBallx = _x-fballOffset
          fBally = _y
        }
        case (false,true,false,false) => {
          _face.chDir(East)
          fBallx = _x+fballOffset
          fBally = _y
        }
        case (false,false,true,false) => {
          _face.chDir(South)
          fBally = _y+fballOffset
          fBallx = _x
        }
        case (false,false,false,true) => {
          _face.chDir(North)
          fBally = _y-fballOffset
          fBallx = _x
        }
        case n => print("") // this should never happen, but it makes the compiler happy
      }
    } else {fBallx = _x; fBally=_y}
    if (spcReleased == true && spcDelay<burstThreshold) { // spawn a fireball with the same face as the player
      level += new Fireball(fBallx,fBally,_face.fClone(this.fBallSpin),level,this)
    } else if (spcReleased == true) {
      // Spawn a special fireball
      level += new FireBurst(fBallx,fBally,_face.fClone(this.fBallSpin),level,this)
    }
    // reset charge delay after firing
    if (spcReleased) spcDelay = 0
  }
  private var healDelay = 0
  private def heal(pts:Int):Unit = {
    val healBuffer = 25
    if(!this.isHurt && this.armor<22 && dirsPressed==0 && healDelay>healBuffer) {
      armor+=1
    }
    healDelay+=1
    if(healDelay>healBuffer+1) healDelay=0
  }
  private var alive = true

  def impact(fFace:OrientationDir.Value, overlapStr:String):Unit = {
    hurt = true
    if(armor<=0){
      alive=false
    }
    overlapStr match {
      case "nonoverlap" => armor -= 2
      case "overlap" => {
        _x = oldx
        _y = oldy
        armor -= 1
      }
      case "burst" => armor -= 5
    }
  }


  override def stillHere(): Boolean = alive

  /**
    The collision detection code below checks
    if the movement is only illegal on one axis,
    and then only correcting only that axis if necessary.

    This avoids issues where the player cannot brush against
    walls with diagonal movement which I find incredibly
    annoying.
    **/

  def wallCollideCheck(boldx:Double,boldy:Double):Unit = {
    if (level.maze.isClear(_x, boldy, 1.0, 1.0) == false && level.maze.isClear(boldx, _y, 1.0, 1.0) == true) _x = boldx
    if (level.maze.isClear(boldx, _y, 1.0, 1.0) == false && level.maze.isClear(_x, boldy, 1.0, 1.0) == true) _y = boldy
    if (level.maze.isClear(boldx, _y, 1.0, 1.0) == false && level.maze.isClear(_x, boldy, 1.0, 1.0) == false) {
      _y = boldy
      _x = boldx
    }
  }
  override def postCheck(): Unit = {
    wallCollideCheck(oldx,oldy)
    spcReleased = false
    dirsPressed = 0
  }

  /**
    Handling keyboard input from Main.
    The Int dirsPressed inside of update and postcheck
    relies on these to tell if the player is only moving
    in one direction to tell whether the player should
    change face versus just strafing.
    **/

  // upon key pressed
  def moveUpP(): Unit = {
    upPressed = true
  }

  def moveDownP(): Unit = {
    downPressed = true
  }

  def moveLeftP(): Unit = {
    leftPressed = true
  }

  def moveRightP(): Unit = {
    rightPressed = true
  }

  // upon key released
  def moveRightR(): Unit = {
    rightPressed = false
  }

  def moveLeftR(): Unit = {
    leftPressed = false
  }

  def moveDownR(): Unit = {
    downPressed = false
  }

  def moveUpR(): Unit = {
    upPressed = false
  }

  /**
    This handles the spacebar, which works a bit differently.
    Holding spacebar a bit before releasing spawns a fireblast.
    **/
  def attackP(): Unit = {
    spcPressed = true
  }

  def attackR(): Unit = {
    spcPressed = false
    spcReleased = true
  }

}
