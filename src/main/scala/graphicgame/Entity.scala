package graphicgame

  /**
    Entities are all moving or interactable objects with sprites.
    They all have a face (direction they are facing),
    x coordinate, y coordinate, width, height values.

    Then update runs after a delay in the entity's Level's
    updateAll() as goes for postCheck and stillHere.
    Still here will allow the level to determine if it should
    keep the entity or not.
    * */

trait Entity {
  /** The direction the entity is facing **/

  def face: OrientationDir.Value

  def x: Double

  def y: Double

  def width(): Double = {
    this.width
  }

  def height(): Double = {
    this.height
  }

  /** The general class of the entity **/
  def entityType(): EntityType.Value

  /** False by default and only applies to entities with attack animations. **/
  def isAttacking():Boolean

  /** False by default and only applies to entites with damage animations (flashing red, etc.)**/
  def isHurt():Boolean

  /** A Per-entity unique name, only really applies to Player entites at the moment **/
  def name():String

  /** A score for the entity, only really applies to Player entities at the moment **/
  def score():Int

  def update(delay: Double): Unit = {}

  def postCheck(): Unit = {}

  def stillHere(): Boolean = {
    true // entities should exist by default haha
  }
  def hp(): Int
}

object EntityType extends Enumeration {
  val Player, Enemy, Swipe, Fireball, FireBurst, Roomba = Value
}
