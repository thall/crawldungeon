package graphicgame

import OrientationDir._

class Swipe(private var _x:Double, private var _y:Double, private var _face:Orientation, level:Level) extends Entity {

  /**
    Swipe is just the small projectile spawned by knights when they
    lunge forward with their sword. Works a ton like fireball just
    without spin. For consistency this also has "extinguish"
    as a function to get rid of the swipe like a fireball.
    **/
  import EntityType._

  private var here = true
  private var distTraveled = 0

  override def x: Double = _x
  override def y: Double = _y
  override def width(): Double = 1.0
  override def height(): Double = 1.0
  override def stillHere(): Boolean = here

  def name:String = "swipe"
  def entityType: EntityType.Value = Swipe
  def face: OrientationDir.Value = _face.dir
  def isAttacking: Boolean = false
  def hp:Int = 0
  def extinguish: Unit = here = false
  def score:Int = 0
  def isHurt: Boolean = false

  // move in the current direction
  override def update(delay: Double): Unit = {
    distTraveled+=1
    if(distTraveled>=1500) this.extinguish
    _face.dir match { // TODO: maybe add code to change sprite width height per case
      case North => _y-=.2
      case South => _y+=.2
      case East => _x+=.2
      case West => _x-=.2
    }
  }

  // what happens when swipe is in a wall after moving
  override def postCheck(): Unit = {
    if(!level.maze.isClear(_x,_y,this.width,this.height)) {
      here=false
    }
  }

}
