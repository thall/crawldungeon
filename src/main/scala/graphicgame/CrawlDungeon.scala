package graphicgame

object CrawlDungeon {
  def main(args: Array[String]):Unit = {
    if(args.length!=1) printUsage()
    val firstArg = args(0)
    firstArg match {
      case "server" => {
        println("--launching server--")
        Server.main(args)
      }
      case "client" => {
        println("--launching client--")
        Client.main(args)
      }
      case _ => printUsage()
    }
  }
  private def printUsage():Unit = {
    println("usage: CrawlDungeon [client/server]")
    System.exit(0)
  }
}
