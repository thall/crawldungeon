package graphicgame

import java.rmi.server.UnicastRemoteObject

import scalafx.Includes._
import scalafx.application.JFXApp
import scalafx.scene.Scene
import scalafx.scene.canvas._
import scalafx.scene.input.{KeyCode, KeyEvent}
import scalafx.application.{JFXApp,Platform}

import scala.io.StdIn._

import EntityType._

object Client extends UnicastRemoteObject with JFXApp with RemoteClient {
  println("Enter a player name, if you just want to spectate it should contain \"spectator\"")
  println("    (something below 7 characters is ideal, but anything is valid)")
  val inputName = readLine
  val name = inputName
  if(name.contains(' ')){
    println("no spaces in the name please.")
    System.exit(0)
  }
  println("enter server hostname/address:")
  val addr = readLine
  val server = java.rmi.Naming.lookup("rmi://"+addr+":1099/CrawlDungeonServer") match {
    case rs: RemoteServer => rs
  }
  val input = server.connect(this) //RemotePlayer

  val canvas = new Canvas(1000, 800)
  val gc = canvas.graphicsContext2D

  def getName():String = {
    name
  }

  def updateLevel(lvl: PassableLevel): Unit ={
    Platform.runLater {
      val results = lvl.entities.filter(e => e.entityType == Player && e.name == name)
      val pAlive = if(results.length>0) true else false
      val (myX, cY) = if(pAlive) {
        val myPlayer = results(0)
        (myPlayer.x,myPlayer.y)
      } else (30.0, 30.0)
      val renderer = if(pAlive) new Renderer2D(gc, 26.0, name) else {
        new Renderer2D(gc, 13.0, name)
      }
      renderer.render(lvl, myX, cY)
    }
  }
  stage = new JFXApp.PrimaryStage {
    title = "Crawl Dungeon: Rock Stew"
    scene = new Scene(1000, 800) {
      content = canvas

      onKeyPressed = (e: KeyEvent) => {
        e.code match {
          case KeyCode.Up => input.moveUpP
          case KeyCode.Down => input.moveDownP
          case KeyCode.Left => input.moveLeftP
          case KeyCode.Right => input.moveRightP
          case KeyCode.Space => input.attackP
          case _ =>
        }
      }
      onKeyReleased = (e: KeyEvent) => {
        e.code match {
          case KeyCode.Up => input.moveUpR
          case KeyCode.Down => input.moveDownR
          case KeyCode.Left => input.moveLeftR
          case KeyCode.Right => input.moveRightR
          case KeyCode.Space => input.attackR
          case _ =>
        }
      }
    }
  }
}
